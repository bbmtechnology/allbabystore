using Elyon.Application.Services.ViewModel;
using Elyon.Domain.Interface;
using Elyon.Domain.Layer;
using Elyon.Domain.Model;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AutoMapper;

namespace TestServiceDomain
{
    [TestClass]
    public class TestAdrres_Service_Domain
    {        
        private readonly IAddressDomainLayer domainLayer;

        public TestAdrres_Service_Domain()
        {            
            domainLayer = new AddressServiceLayer();
        }

        [TestMethod]
        public void Save()
        {
            var model = new AddressViewModel()
            {
                City = "Campanario",
                CodeInternal = "END001",
                Complement = "Rua dos cortinhtias",
                Country = "Brasil",
                Initials = "UF",
                Note = "nenhum",
                Street = "Rua geoergs lacome",
                State = "S�o Paulo",
                ZipCode = "04173010"
            };
              
            var result = Mapper.Map<Address>(model);           


        }
    }
}

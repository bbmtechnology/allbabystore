﻿using Elyon.Application.ViewModel.ViewModel;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Elyon.Application.Services.Interface
{
    public interface IContactAppService
    {
        IReadOnlyList<ContactViewModel> GetAllContactReadOnly();
        IEnumerable<ContactViewModel> GetContactEnumerable();
        ContactViewModel Create(ContactViewModel contact);
        ContactViewModel Update(ContactViewModel contact);
        Task<ContactViewModel> CreateAsync(ContactViewModel contact);
        bool Delete(Guid Id);
    }
}
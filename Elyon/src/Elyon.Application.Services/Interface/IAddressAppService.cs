﻿using Elyon.Application.Services.ViewModel;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Elyon.Application.Services.Interface
{
    public interface IAddressAppService
    {
        IReadOnlyList<AddressViewModel> GetAllAdressesReadOnly();
        IEnumerable<AddressViewModel> GetAddressesEnumerable();
        AddressViewModel Create(AddressViewModel address);
        AddressViewModel Update(AddressViewModel address);
        Task<AddressViewModel> CreateAsync(AddressViewModel address);
        bool Delete(Guid Id);
        AddressViewModel GetAddressByZipCode(string zipcode);
    }
}
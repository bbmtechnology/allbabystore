﻿using AutoMapper;
using Elyon.Application.Services.Interface;
using Elyon.Application.Services.ViewModel;
using Elyon.Domain.CommandHelper;
using Elyon.Domain.Enumerators;
using Elyon.Domain.Interface;
using Elyon.Domain.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Threading.Tasks;

namespace Elyon.Application.Services.Service
{
    public class AddressApplicationService : IAddressAppService
    {
        private readonly IAddressDomainLayer _addressDomainLayer;
        private readonly IMapper _mapper;

        public AddressApplicationService
            (IAddressDomainLayer addressDomainLayer, IMapper mapper)
        {
            _addressDomainLayer = addressDomainLayer;
            _mapper = mapper;
        }

        public bool Delete(Guid Id) => _addressDomainLayer.Remove(Id);
        public AddressViewModel Create(AddressViewModel address)
        {
            return _mapper
                     .Map<AddressViewModel>(
                       _addressDomainLayer.Create(
                         _mapper
                          .Map<Address>
                             (address)));
        }
        public async Task<AddressViewModel> CreateAsync(AddressViewModel address)
        {
            return _mapper
                    .Map<AddressViewModel>(
                       await _addressDomainLayer.CreateAsync(
                         _mapper
                          .Map<Address>
                              (address)));
        }
        public IEnumerable<AddressViewModel> GetAddressesEnumerable()
        {
            return _mapper
                     .Map<IEnumerable<AddressViewModel>>(
                        _addressDomainLayer.GetAddresses());
        }
        public IReadOnlyList<AddressViewModel> GetAllAdressesReadOnly()
        {
            return _mapper
                     .Map<IReadOnlyList<AddressViewModel>>(
                       _addressDomainLayer.GetAddressesReadOnLy());
        }
        public AddressViewModel Update(AddressViewModel address)
        {
            return _mapper
                     .Map<AddressViewModel>(
                       _addressDomainLayer.AlterAddress(
                         _mapper
                          .Map<Address>
                             (address)));
        }
        public AddressViewModel GetAddressByZipCode(string zipcode)
        {
            if (!HelperTools.CepIsValid(zipcode.Replace("-", ""))) return null;
            string _zipCode = zipcode.Trim().Replace("-", "");
            string endPoint = $"https://viacep.com.br/ws/{_zipCode}/json/";

            HttpWebRequest request = WebRequest.Create(endPoint) as HttpWebRequest;
            request.Method = EHttpVerb.GET.ToString();

            using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
            {
                if (response.StatusCode != HttpStatusCode.OK)
                {
                    throw new ApplicationException("error code: " + response.StatusCode);
                }
                using (Stream responseStream = response.GetResponseStream())
                {
                    if (responseStream != null)
                    {
                        using (StreamReader reader = new StreamReader(responseStream))
                        {
                            var result = JsonConvert.DeserializeObject<AddressViewModel>(reader.ReadToEnd());
                            result.Complement = null;
                            result.State = null;
                            return result;
                        }
                    }
                }
            }
            return null;
        }
    }
}
﻿using AutoMapper;
using Elyon.Application.Services.Interface;
using Elyon.Application.ViewModel.ViewModel;
using Elyon.Domain.Interface;
using Elyon.Domain.Model;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Elyon.Application.Services.Service
{
    public class ContactApplicationService : IContactAppService
    {
        private readonly IContactDomainLayer _contactDomainLayer;
        private readonly IMapper _mapper;

        public ContactApplicationService
        (IMapper mapper, IContactDomainLayer contactDomainLayer)
        {
            _contactDomainLayer = contactDomainLayer;
            _mapper = mapper;
        }

        public ContactViewModel Create(ContactViewModel contact)
        {
            return _mapper
                      .Map<ContactViewModel>(
                        _contactDomainLayer.Create(
                          _mapper
                           .Map<Contact>
                              (contact)));
        }

        public async Task<ContactViewModel> CreateAsync(ContactViewModel contact)
        {
            return _mapper
                 .Map<ContactViewModel>(
                    await _contactDomainLayer.CreateAsync(
                      _mapper
                       .Map<Contact>
                           (contact)));
        }

        public bool Delete(Guid Id) => _contactDomainLayer.Remove(Id);

        public IReadOnlyList<ContactViewModel> GetAllContactReadOnly()
        {
            return _mapper
                    .Map<IReadOnlyList<ContactViewModel>>(
                      _contactDomainLayer.GetContactsReadOnLy());
        }

        public IEnumerable<ContactViewModel> GetContactEnumerable()
        {
            return _mapper
                    .Map<IEnumerable<ContactViewModel>>(
                       _contactDomainLayer.GetContactsEnumerable());
        }

        public ContactViewModel Update(ContactViewModel contact)
        {
            return _mapper
                     .Map<ContactViewModel>(
                       _contactDomainLayer.AlterContact(
                         _mapper
                          .Map<Contact>
                             (contact)));
        }
    }
}

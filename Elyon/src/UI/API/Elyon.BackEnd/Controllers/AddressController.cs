﻿using Elyon.Application.Services.Interface;
using Elyon.Application.Services.ViewModel;
using Elyon.BackEnd.Controllers.Base;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace Elyon.BackEnd.Controllers
{
    [Route("api/v1/address")]
    [ApiController]
    public class AddressController : ApiController
    {
        private IAddressAppService _AddressControlle;

        public AddressController
        (IAddressAppService addressAppService)
        {
            _AddressControlle = addressAppService; 
        }

        [HttpGet]
        public IActionResult Get()
        {
            return Response(_AddressControlle.GetAllAdressesReadOnly());
        }

        [HttpPost]
        public IActionResult Post([FromBody]AddressViewModel address)
        {
            return Created(_AddressControlle.Create(address));
        }

        [HttpPost, Route("create-async")]        
        public async Task<IActionResult> PostAsync([FromBody]AddressViewModel address)
        {
            return Created(await _AddressControlle.CreateAsync(address));
        }
        
        [HttpDelete, Route("{id}")]
        public IActionResult Delete(Guid Id)
        {
            return Response(_AddressControlle.Delete(Id));
        }

        [HttpPut]
        public IActionResult Put([FromBody]AddressViewModel address)
        {
            return Response(_AddressControlle.Update(address));
        }
       
        [HttpGet, Route("get-by-zipcode/{id}")]
        public IActionResult GetAddressById(string Id)
        {
            return Response(_AddressControlle.GetAddressByZipCode(Id));
        }
    }
}
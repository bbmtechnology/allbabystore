﻿using Elyon.BackEnd.Controllers.Base;
using Microsoft.AspNetCore.Mvc;

namespace Elyon.BackEnd.Controllers
{
    [Route("api/v1/default")]
    [ApiController]
    public class DefaultController : ControllerBase
    {
        [HttpGet]
        public ActionResult Index()
        {
            return Ok("{Bem vindo}"); ;
        }
    }
}
﻿using Elyon.Domain.Command;
using Elyon.Domain.Interface;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;

namespace Elyon.BackEnd.Controllers.Base
{
    public class ApiController : ControllerBase
    {
        private readonly INotification _notifications;
        protected ApiController()
        {
            _notifications = new Notification();
            _notifications.Init();
        }

        protected bool IsValidOperation()
        {
            return (!_notifications.HasNotifications());
        }

        protected new IActionResult Response(object result = null)
        {
            try
            {

                if (result == null)
                {
                    return NoContent();
                }


                if (IsValidOperation())
                {
                    return Ok(new
                    {
                        success = true,
                        data = result
                    });
                }

                return BadRequest(new
                {
                    success = false,
                    errors = _notifications.Notifications().Select(n => n.Value)
                });
            }
            catch (Exception ex)
            {
                return StatusCode(500, "Ocorreu um erro interno.");
            }
        }

        protected IActionResult Created(object result = null)
        {
            try
            {
                if (result == null)
                {
                    return NoContent();
                }

                if (IsValidOperation())
                {
                    return StatusCode(201, result);
                }

                return BadRequest(new
                {
                    success = false,
                    errors = _notifications.Notifications().Select(n => n.Value)
                });
            }
            catch (Exception ex)
            {
                return StatusCode(500, "Ocorreu um erro interno.");
            }
        }
    }
}
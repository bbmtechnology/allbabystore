﻿using Elyon.Application.Services.Interface;
using Elyon.Application.ViewModel.ViewModel;
using Elyon.BackEnd.Controllers.Base;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace Elyon.BackEnd.Controllers
{
    [Route("api/v1/contact")]
    [ApiController]
    public class ContactController : ApiController
    {
        private IContactAppService _ContactControlle;

        public ContactController
        (IContactAppService contactAppService)
        {
            _ContactControlle = contactAppService;
        }

        [HttpGet]
        public IActionResult Get()
        {
            return Response(_ContactControlle.GetAllContactReadOnly());
        }

        [HttpPost]
        public IActionResult Post([FromBody]ContactViewModel contact)
        {
            return Created(_ContactControlle.Create(contact));
        }

        [HttpPost, Route("create-async")]
        public async Task<IActionResult> PostAsync([FromBody]ContactViewModel contact)
        {
            return Created(await _ContactControlle.CreateAsync(contact));
        }

        [HttpDelete, Route("{id}")]
        public IActionResult Delete(Guid Id)
        {
            return Response(_ContactControlle.Delete(Id));
        }

        [HttpPut]
        public IActionResult Put([FromBody]ContactViewModel contact)
        {
            return Response(_ContactControlle.Update(contact));
        }
    }
}
﻿using AutoMapper;
using Elyon.Application.AutoMapper.Config;
using Elyon.Application.AutoMapper.ModelToView;
using Elyon.Application.AutoMapper.ViewToModel;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace Elyon.BackEnd.Configuration
{
    public static class AutoMapperSetup
    {

        public static void AddAutoMapperSetup(this IServiceCollection services)
        {
            if (services == null) throw new ArgumentNullException(nameof(services));
            services.AddAutoMapper();
            AutoMapperConfig.RegisterMappings();                       
        }

    }
}
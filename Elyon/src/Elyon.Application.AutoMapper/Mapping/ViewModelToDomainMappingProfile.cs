﻿using AutoMapper;
using Elyon.Application.Services.ViewModel;
using Elyon.Application.ViewModel.ViewModel;
using Elyon.Domain.Model;

namespace Elyon.Application.AutoMapper.ModelToView
{
    public class ViewModelToDomainMappingProfile : Profile
    {
        public ViewModelToDomainMappingProfile()
        {
            //Dê ViewModel para Domain
            CreateMap<AddressViewModel, Address>()
            .ForPath(m => m.GuidId,       vm => vm.MapFrom(a => a.Id))
            .ForPath(m => m.InternalCode, vm => vm.MapFrom(a => a.CodeInternal))
            .ForPath(m => m.Street,       vm => vm.MapFrom(a => a.Street))
            .ForPath(m => m.City,         vm => vm.MapFrom(a => a.City))
            .ForPath(m => m.Complement,   vm => vm.MapFrom(a => a.Complement))
            .ForPath(m => m.Country,      vm => vm.MapFrom(a => a.Country))
            .ForPath(m => m.ZipCode,      vm => vm.MapFrom(a => a.ZipCode))
            .ForPath(m => m.Reference,    vm => vm.MapFrom(a => a.Reference))
            .ForPath(m => m.EStatus,      vm => vm.MapFrom(a => a.EStatusRegister))
            .ForPath(m => m.Initials,     vm => vm.MapFrom(a => a.Initials))
            .ForPath(m => m.District,     vm => vm.MapFrom(a => a.Distric))
            .ForPath(m => m.HouseNumber,  vm => vm.MapFrom( a => a.HouseNumber))
            .ForPath(m => m.RecordsHistory.CreationDate, vm => vm.MapFrom(a => a.DateCreation))
            .ForPath(m => m.RecordsHistory.ChangeDate,   vm => vm.MapFrom(a => a.DateUpdate))
            .ReverseMap();

            //Dê ViewModel para Domain
            CreateMap<ContactViewModel,Contact>()
              .ForPath(m => m.GuidId,           vm => vm.MapFrom(m => m.Id))
              .ForPath(m => m.InternalCode, vm => vm.MapFrom(m => m.CodeInternal))
              .ForPath(m => m.CellPhone,    vm => vm.MapFrom(m => m.CellPhone))
              .ForPath(m => m.DDDCellPhone, vm => vm.MapFrom(m => m.DDD_CellPhone))
              .ForPath(m => m.Phone,        vm => vm.MapFrom(m => m.Phone))
              .ForPath(m => m.DDDPhone,     vm => vm.MapFrom(m => m.DDD_Phone))
              .ForPath(m => m.Email,        vm => vm.MapFrom(m => m.Email))
              .ForPath(m => m.PersonName,   vm => vm.MapFrom(m => m.PersonName))
              .ForPath(m => m.EStatus,      vm => vm.MapFrom(m => m.EStatusRegister))
              .ForPath(m => m.RecordsHistory.CreationDate, vm => vm.MapFrom(a => a.DateCreation))
              .ForPath(m => m.RecordsHistory.ChangeDate,   vm => vm.MapFrom(a => a.DateUpdate))
              .ReverseMap();
        }
    }
}
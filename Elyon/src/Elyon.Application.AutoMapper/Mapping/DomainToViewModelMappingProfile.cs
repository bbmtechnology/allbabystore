﻿using AutoMapper;
using Elyon.Application.Services.ViewModel;
using Elyon.Application.ViewModel.ViewModel;
using Elyon.Domain.Model;

namespace Elyon.Application.AutoMapper.ViewToModel
{
    public class DomainToViewModelMappingProfile : Profile
    {
        public DomainToViewModelMappingProfile()
        {
            //Dê AddressDomain para AddresViewModel
            CreateMap<Address, AddressViewModel>()                  
             .ForPath(vm => vm.Id,        dm => dm.MapFrom(m => m.GuidId))
             .ForPath(vm => vm.CodeInternal, dm => dm.MapFrom(m => m.InternalCode))
             .ForPath(vm => vm.Street,       dm => dm.MapFrom(m => m.Street))
             .ForPath(vm => vm.City,         dm => dm.MapFrom(m => m.City))
             .ForPath(vm => vm.Complement,   dm => dm.MapFrom(m => m.Complement))
             .ForPath(vm => vm.Country,      dm => dm.MapFrom(m => m.Country))
             .ForPath(vm => vm.ZipCode,      dm => dm.MapFrom(m => m.ZipCode))
             .ForPath(vm => vm.Reference,    dm => dm.MapFrom(m => m.Reference))
             .ForPath(vm => vm.EStatusRegister, dm => dm.MapFrom(m => m.EStatus))
             .ForPath(vm => vm.Initials,        dm => dm.MapFrom(m => m.Initials))
             .ForPath(vm => vm.Distric,         dm => dm.MapFrom(m => m.District))
             .ForPath(vm => vm.DateCreation,    dm => dm.MapFrom(m => m.RecordsHistory.CreationDate))
             .ForPath(vm => vm.DateUpdate,      dm => dm.MapFrom(m => m.RecordsHistory.ChangeDate))
             .ForPath(vm => vm.HouseNumber,     dm => dm.MapFrom(m => m.HouseNumber))
             .ReverseMap();

            //Dê ContactDomain para ContactViewModel
            CreateMap<Contact, ContactViewModel>()
              .ForPath(vm => vm.Id,             dm => dm.MapFrom(m => m.GuidId))
              .ForPath(vm => vm.CodeInternal,   dm => dm.MapFrom(m => m.InternalCode))
              .ForPath(vm => vm.CellPhone,      dm => dm.MapFrom(m => m.CellPhone))
              .ForPath(vm => vm.DDD_CellPhone,  dm => dm.MapFrom(m => m.DDDCellPhone))
              .ForPath(vm => vm.Phone,          dm => dm.MapFrom(m => m.Phone))
              .ForPath(vm => vm.DDD_Phone,      dm => dm.MapFrom(m => m.DDDPhone))
              .ForPath(vm => vm.Email,          dm => dm.MapFrom(m => m.Email))
              .ForPath(vm => vm.PersonName,     dm => dm.MapFrom(m => m.PersonName))
              .ForPath(vm => vm.DateCreation,   dm => dm.MapFrom(m => m.RecordsHistory.CreationDate))
              .ForPath(vm => vm.DateUpdate,     dm => dm.MapFrom(m => m.RecordsHistory.ChangeDate))
              .ForPath(vm => vm.EStatusRegister,dm => dm.MapFrom(m => m.EStatus))
              .ReverseMap();
        }
    }
}
﻿using AutoMapper;
using Elyon.Application.AutoMapper.ModelToView;
using Elyon.Application.AutoMapper.ViewToModel;

namespace Elyon.Application.AutoMapper.Config
{
    public class AutoMapperConfig
    {
        public static MapperConfiguration RegisterMappings()
        {
            return new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new DomainToViewModelMappingProfile());
                cfg.AddProfile(new ViewModelToDomainMappingProfile());
            });
        }
    }
}
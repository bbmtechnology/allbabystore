﻿using Elyon.Domain.Enumerators;
using Newtonsoft.Json;
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Elyon.Application.Services.ViewModel
{
    public class AddressViewModel
    {
        [Key]
        public Guid Id { get; set; }

        [Key]
        public string CodeInternal { get; set; }        
        
        [Required(ErrorMessage = "Informe o nome do logradouro.")]
        [MinLength(1)]
        [MaxLength(300)]
        [DisplayName("Logradouro")]
        [JsonProperty(PropertyName = "logradouro")]
        public string Street { get; set; }
        
        [Required(ErrorMessage = "Informe o nome da cidade.")]
        [MinLength(1)]
        [MaxLength(100)]
        [DisplayName("Cidade")]
        [JsonProperty(PropertyName = "localidade")]
        public string City { get; set; }

        [Required(ErrorMessage = "Informe o nome do bairro.")]
        [MinLength(1)]
        [MaxLength(100)]
        [DisplayName("Bairro")]
        [JsonProperty(PropertyName = "bairro")]
        public string Distric { get; set; }

        [Required(ErrorMessage = "Informe o nome do país.")]
        [MinLength(1)]
        [MaxLength(100)]
        [DisplayName("País")]
        [JsonProperty(PropertyName = "pais")]
        public string Country { get; set; }
        
        [Required(ErrorMessage = "Informe o nome do estado.")]
        [MinLength(1)]
        [MaxLength(100)]
        [DisplayName("Estado")]
        [JsonProperty(PropertyName = "estado")]
        public string State { get; set; }
        
        [Required(ErrorMessage = "Insira os dados do CEP.")]
        [MinLength(8)]
        [MaxLength(9)]
        [DisplayName("CEP")]
        [JsonProperty(PropertyName = "cep")]
        public string ZipCode { get; set; }
        
        [Required(ErrorMessage = "Informe a UF do estado correspondente.")]
        [MinLength(2)]
        [MaxLength(2)]
        [DisplayName("UF")]
        [JsonProperty(PropertyName = "uf")]
        public string Initials { get; set; }

        [MaxLength(100)]
        [DisplayName("Complemento")]
        [JsonProperty(PropertyName = "complemento")]
        public string Complement { get; set; }

        [MaxLength(100)]
        [DisplayName("Referencia")]
        [JsonProperty(PropertyName = "referencia")]
        public string Reference { get; set; }

        [MaxLength(100)]
        [DisplayName("Observação")]
        [JsonProperty(PropertyName = "observacao")]
        public string Note { get; set; }

        [DisplayName("Status")]
        [JsonProperty(PropertyName = "status")]
        public EStatusRegister EStatusRegister { get; private set; }

        [DisplayName("Data criação")]
        [JsonProperty(PropertyName = "date-creation")]
        public string DateCreation { get; private set; }

        [DisplayName("Ultima atualização")]
        [JsonProperty(PropertyName = "date-update")]
        public string DateUpdate { get; private set; }
        
        [DisplayName("Número")]
        [JsonProperty(PropertyName="numero")]
        public string HouseNumber { get; set; }

        public AddressViewModel()
        {
            EStatusRegister = EStatusRegister.Active;
        }
    }
}
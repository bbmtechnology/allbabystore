﻿using Elyon.Domain.Enumerators;
using Newtonsoft.Json;
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Elyon.Application.ViewModel.ViewModel
{
    public class ContactViewModel
    {
        [Key]
        public Guid Id { get; set; }

        [Key]
        public string CodeInternal { get; set; }

        [Required(ErrorMessage = "Informe o E-mail.")]
        [MinLength(1)]
        [MaxLength(300)]
        [DisplayName("E-mail")]
        [DataType(DataType.EmailAddress)]
        [EmailAddress]
        [JsonProperty(PropertyName = "e-mail")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Insira um número de telefone.")]
        [MinLength(8)]
        [MaxLength(9)]
        [DisplayName("Telefone")]
        [JsonProperty(PropertyName = "phone")]
        public string Phone { get; set; }

        [Required(ErrorMessage = "Insira o DDD do telefone.")]
        [MinLength(2)]
        [MaxLength(4)]
        [DisplayName("DDD")]
        [JsonProperty(PropertyName = "ddd-phone")]
        public string DDD_Phone { get; set; }

        [Required(ErrorMessage = "Insira um número de celular ou envie 9 digitos zero.")]
        [MinLength(8)]
        [MaxLength(9)]
        [DisplayName("Celular")]
        [JsonProperty(PropertyName = "cellphone")]
        public string CellPhone { get; set; }

        [Required(ErrorMessage = "Insira o DDD para o celular ou envie 2 digitos zero.")]
        [MinLength(2)]
        [MaxLength(4)]
        [DisplayName("DDD")]
        [JsonProperty(PropertyName = "ddd-cellphone")]
        public string DDD_CellPhone { get; set; }

        [Required(ErrorMessage = "Insira o nome do titular deste contato.")]
        [MinLength(1)]
        [MaxLength(100)]
        [DisplayName("Nome")]
        [JsonProperty(PropertyName = "person-name")]
        public string PersonName { get; set; }

        [DisplayName("Data criação")]
        [JsonProperty(PropertyName = "date-creation")]
        public string DateCreation { get; private set; }

        [DisplayName("Ultima atualização")]
        [JsonProperty(PropertyName = "date-update")]
        public string DateUpdate { get; private set; }

        [DisplayName("Status")]
        [JsonProperty(PropertyName = "status")]
        public EStatusRegister EStatusRegister { get; set; }

        public ContactViewModel()
        {
            EStatusRegister = EStatusRegister.Active;
        }
    }
}
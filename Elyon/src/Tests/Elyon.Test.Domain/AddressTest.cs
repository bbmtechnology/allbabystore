﻿using Elyon.Application.Services.Interface;
using Elyon.Application.Services.ViewModel;
using Elyon.Domain.Command;
using Elyon.Domain.Interface;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Elyon.Test.Domain
{
    [TestClass]
    public class AddressTest
    {
        private IAddressAppService _AddressControlle;
        private readonly INotification _notifications;

        public AddressTest(IAddressAppService addressAppService)
        {
            _AddressControlle = addressAppService;
            _notifications = new Notification();
            _notifications.Init();
        }

        protected bool IsValidOperation()
        {
            return (!_notifications.HasNotifications());
        }


        [TestMethod]
        public void Create_Address_Sucess()
        {
            var model = new AddressViewModel()
            {
                City = "Campanario",
                CodeInternal = "END001",
                Complement = "Rua dos cortinhtias",
                Country = "Brasil",
                Initials = "UF",
                Note = "nenhum",
                Street = "Rua geoergs lacome",
                State = "São Paulo",
                ZipCode = "04173010"
            };

            _AddressControlle.Create(model);

            Assert.IsTrue(IsValidOperation());
        }

        [TestMethod]
        public void Create_Address_Failed()
        {
            var model = new AddressViewModel()
            {
                City = "",
                CodeInternal = "",
                Complement = "",
                Country = "",
                Initials = "",
                Note = "",
                Street = "",
                State = "",
                ZipCode = ""
            };

            _AddressControlle.Create(model);

            Assert.IsTrue(!IsValidOperation());
        }

    }
}

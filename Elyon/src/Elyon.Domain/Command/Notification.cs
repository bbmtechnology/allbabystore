﻿using Elyon.Domain.Interface;
using Elyon.Domain.Notification.Entitie;
using System;
using System.Collections.Generic;

namespace Elyon.Domain.Command
{
    public sealed class Notification : INotification, IDisposable
    {
        private static List<Message> _listNotification { get; set; }

        public Notification()
        {
            if(_listNotification == null)
            _listNotification = new List<Message>();
        }

        public void Add(string message) => _listNotification.Add(new Message() { Value = message });
        public List<Message> Notifications() => _listNotification;
        public bool HasNotifications() => _listNotification.Count > 0;
        public void Init()
        {
            if (_listNotification.Count > 0)
                _listNotification.Clear();
        }             
        public void Dispose()
        {
            if (_listNotification.Count > 0)
                _listNotification.Clear();
            GC.SuppressFinalize(this);
        }
    }
}
﻿using Elyon.Domain.CommandHelper;
using Elyon.Domain.Interface;
using Elyon.Domain.Model;

namespace Elyon.Domain.Command
{
    public class OnValidateDomain : IOnInit
    {
        private bool ResultInitialize { get; set; }
        protected readonly INotification _notification;

        public OnValidateDomain(Contact contact)
        {
            int count = 0;
            if (_notification == null)
                _notification = new Notification();

            if(contact == null)
            {
                _notification.Add("Contato - Objeto vazio, certifique-se de ter enviado os dados do contato.");
                count++;
            }
            if(string.IsNullOrEmpty(contact.PersonName.Trim()))
            {
                _notification.Add("Contato - Insira o nome do titular deste contato.");
                count++;
            }
            if (string.IsNullOrEmpty(contact.Phone.Trim()))
            {
                _notification.Add("Contato - Insira o número do titular deste contato..");
                count++;
            }           
            ResultInitialize = count == 0;
        }


        public OnValidateDomain(Address address)
        {
            int count = 0;
            if (_notification == null)
                _notification = new Notification();
            
            if (address == null)
            {
                _notification.Add("Endereço - Objeto vazio, certifique-se de ter enviado os dados do endereço.");
                count++;
            }
            if (string.IsNullOrEmpty(address.Street) || string.IsNullOrWhiteSpace(address.State))
            {
                _notification.Add("Endereço - Insira o nome do logradouro.");
                count++;
            }
            if (string.IsNullOrEmpty(address.District) || string.IsNullOrWhiteSpace(address.District))
            {
                _notification.Add("Endereço - Insira o nome do bairro.");
                count++;
            }
            if (string.IsNullOrEmpty(address.City) || string.IsNullOrWhiteSpace(address.City))
            {
                _notification.Add("Endereço - Insira o nome da Cidade.");
                count++;
            }
            if (string.IsNullOrEmpty(address.Country) || string.IsNullOrWhiteSpace(address.Country))
            {
                _notification.Add("Endereço - Insira o nome do País.");
                count++;
            }
            if (string.IsNullOrEmpty(address.Initials) || string.IsNullOrWhiteSpace(address.Initials))
            {
                _notification.Add("Endereço - Insira a UF do estado correpondente.");
                count++;
            }
            else if (address.Initials.Length > 2 || address.Initials.Length < 2)
            {
                _notification.Add("Endereço - Limite maximo ultrapassado, o campo UF deve conter 2 caracteres não númerico.");
                count++;
            }
            if (string.IsNullOrEmpty(address.State) || string.IsNullOrWhiteSpace(address.State))
            {
                _notification.Add("Endereço - Insira o Estado.");
                count++;
            }
            if (string.IsNullOrEmpty(address.ZipCode) || string.IsNullOrWhiteSpace(address.ZipCode))
            {
                _notification.Add("Endereço - Insira o CEP do logradouro.");
                count++;
            }
            else
            {
                if (!HelperTools.CepIsValid(address.ZipCode.Replace("-", "")))
                {
                    _notification.Add("Endereço - Insira um CEP valido");
                    count++;
                }              
            }

            ResultInitialize = count == 0;
        }

        public bool DomainIsValid()
        {
            return ResultInitialize;
        }
    }
}
﻿
using Elyon.Domain.Notification.Entitie;
using System.Collections.Generic;

namespace Elyon.Domain.Interface
{
    public interface INotification
    {
        void Add(string message);
        List<Message> Notifications();
        bool HasNotifications();
        void Dispose();
        void Init();
    }
}
﻿using Elyon.Domain.Model;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Elyon.Domain.Interface
{
    public interface IAddressDomainLayer
    {
        bool Remove(Guid Id);
        Address Create(Address address);
        Task<Address> CreateAsync(Address address);
        IEnumerable<Address> GetAddresses();
        IReadOnlyList<Address> GetAddressesReadOnLy();
        Address AlterAddress(Address address);  
    }
}

﻿using Elyon.Domain.Model;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Elyon.Domain.Interface
{
    public interface IContactDomainLayer 
    {
        bool Remove(Guid Id);
        Contact Create(Contact contact);
        Task<Contact> CreateAsync(Contact contact);
        IEnumerable<Contact> GetContactsEnumerable();
        IReadOnlyList<Contact> GetContactsReadOnLy();
        Contact AlterContact(Contact contact);
    }
}

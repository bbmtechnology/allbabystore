﻿using Elyon.Domain.Model;

namespace Elyon.Domain.Interface.Repository
{
    public interface IAddresRepository : IRepository<Address>
    {
    }
}

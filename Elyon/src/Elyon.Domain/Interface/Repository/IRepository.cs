﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Elyon.Domain.Interface.Repository
{
    public interface IRepository<TEntity> : IDisposable where TEntity : class
    {
        #region Sync
        void Add(TEntity obj);
        TEntity GetById(int id);
        IQueryable<TEntity> GetAll();
        IQueryable<TEntity> GetAllNoTracking();
        void Update(TEntity obj);
        void Remove(int id);
        int Save();
        #endregion

        #region Async
        Task AddAsync(TEntity obj);
        Task<int> SaveAsync();
        #endregion

    }
}
﻿namespace Elyon.Domain.ValuesObjects
{
    public class DocumentVO
    {
        public string CNPJ { get; set; }
        public string CPF { get; set; }
        public string InscriçãoEstadual { get; set; }
        public string IncriçãoMunicipal { get; set; }
    }
}
﻿using System;

namespace Elyon.Domain.ValuesObjects
{
    public class Records
    {
        public DateTime CreationDate { get; set; }
        public DateTime ChangeDate { get; set; }
    }
}
﻿using Elyon.Domain.Enumerators;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace Elyon.Domain.CommandHelper
{
    public static class HelperTools
    {
        public static bool CepIsValid(string value)
        {
            //return Regex.IsMatch(value, "[0-9]{5}-[0-9]{3}");
            return Regex.IsMatch(value, "[0-9]");
        }

        public static bool CepIsValid(int value)
        {
            return Regex.IsMatch(value.ToString(), "[0-9]{5}-[0-9]{3}");
        }

        public static string CodeGenerate(ETypeDomain EType)
        {
                                  

            string code = string.Empty;

            switch (EType)
            {
                case ETypeDomain.Address:
                    code = "END001";
                    break;
                case ETypeDomain.Contact:
                    code = "CTT001";
                    break;
            }

            

            return code;
        }

    }
}

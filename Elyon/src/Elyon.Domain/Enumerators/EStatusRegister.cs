﻿namespace Elyon.Domain.Enumerators
{
    public enum EStatusRegister
    {
        Active,
        Desactive,
        Removed
    }
}

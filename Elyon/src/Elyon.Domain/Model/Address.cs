﻿using Elyon.Domain.Command;
using Elyon.Domain.CommandHelper;
using Elyon.Domain.Enumerators;

namespace Elyon.Domain.Model
{
    public class Address : Base
    {
        public string Street { get; protected set; }
        public string City { get; protected set; }
        public string Country { get; protected set; }
        public string District { get; protected set; }
        public string State { get; protected set; }
        public string ZipCode { get; protected set; }
        public string Complement { get; protected set; }
        public string Reference { get; protected set; }
        public string Note { get; protected set; }
        public string Initials { get; protected set; }
        public string HouseNumber { get; protected set; }

        public Address() { }

        public Address(Address address) : base()
        {
            ModelOk = new OnValidateDomain(address).DomainIsValid();
            if (!ModelOk) return;

            Street = address.Street;
            City = address.City;
            Country = address.Country;
            State = address.State;
            FormatCep(address.ZipCode);
            Complement = address.Complement;
            Reference = address.Reference;
            Note = address.Note;
            Initials = address.Initials;
            HouseNumber = address.HouseNumber;
            District = address.District;
            address.GuidId = GuidId;
            InternalCode = HelperTools.CodeGenerate(ETypeDomain.Address);
        }

        public void FormatCep(string value)
        {
            ZipCode = value.Trim().Replace("-", "");
        }
    }
}
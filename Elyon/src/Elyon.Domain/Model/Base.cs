﻿using Elyon.Domain.Enumerators;
using Elyon.Domain.ValuesObjects;
using System;

namespace Elyon.Domain.Model
{
    public abstract class Base : IDisposable
    {
        public int Id { get; protected set; }
        public string InternalCode { get; protected set; }
        public Guid GuidId { get; protected set; }
        public Records RecordsHistory { get; set; }
        public EStatusRegister EStatus { get; set; }

        protected bool ModelOk { get; set; }

        public Base()
        {
            Id = 0;
            GuidId = Guid.NewGuid();
            EStatus = EStatusRegister.Active;
            RecordsHistory = new Records() { CreationDate = DateTime.Now, ChangeDate = DateTime.Now };
        }

        public virtual bool IsValid()
        {
            return ModelOk;
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }

        public void ChanceId(int value)
        {
            Id = value;
        }
    }
}
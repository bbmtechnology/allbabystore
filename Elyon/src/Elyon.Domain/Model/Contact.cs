﻿using Elyon.Domain.Command;
using Elyon.Domain.CommandHelper;
using Elyon.Domain.Enumerators;

namespace Elyon.Domain.Model
{
    public class Contact : Base
    {
        public string Email { get; protected set; }
        public string Phone { get; protected set; }
        public string DDDPhone { get; protected set; }
        public string CellPhone { get; protected set; }
        public string DDDCellPhone { get; protected set; }
        public string PersonName { get; protected set; }

        public Contact()
        {

        }

        public Contact(Contact contact)
        {
            ModelOk = new OnValidateDomain(contact).DomainIsValid();
            if (!ModelOk) return;
            Email = contact.Email;
            Phone = contact.Phone;
            DDDPhone = contact.DDDPhone;            
            PersonName = contact.PersonName;
            contact.GuidId = GuidId;

            if (string.IsNullOrEmpty(contact.DDDCellPhone.Trim()))
                DDDCellPhone = contact.DDDCellPhone = "00";
            if (string.IsNullOrEmpty(contact.CellPhone.Trim()))
                CellPhone = contact.CellPhone = "000000000";

            DDDCellPhone = contact.DDDCellPhone;
            CellPhone = contact.CellPhone;
            contact.InternalCode = InternalCode = HelperTools.CodeGenerate(ETypeDomain.Contact);            
        }      
    }
}
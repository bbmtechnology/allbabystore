﻿using System.Collections.Generic;

namespace Elyon.Domain.Model
{
    public class Company : Base
    {
        public string CompanyName { get; protected set; }
        public List<Store> Affiliates { get; protected set; }        
    }
}
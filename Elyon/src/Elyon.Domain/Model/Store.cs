﻿using Elyon.Domain.ValuesObjects;

namespace Elyon.Domain.Model
{
    public class Store : Base
    {
        public string Name { get; protected set; }
        public string NickName { get; protected set; }
        public DocumentVO Documents { get; set; }
        public Address Andress { get; set; }
        public Contact Contact { get; set; }

        public Store(Store store)
        {

        }
    }
}
﻿using Elyon.Domain.Command;
using Elyon.Domain.Enumerators;
using Elyon.Domain.Interface;
using Elyon.Domain.Interface.Repository;
using Elyon.Domain.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Elyon.Domain.Layer
{
    public class ContactServiceLayer : IContactDomainLayer
    {
        private readonly IContactRepository _contactRepository;
        private IQueryable<Contact> ContactsDefault { get; set; }
        private IQueryable<Contact> Contacts { get; set; }
        protected readonly INotification _notification;

        public ContactServiceLayer(IContactRepository contact)
        {
            _contactRepository = contact;

            ContactsDefault = _contactRepository
                .GetAllNoTracking()
                .Where(x => x.EStatus.Equals(EStatusRegister.Active) || x.Equals(EStatusRegister.Desactive));

            Contacts = _contactRepository.GetAll();

            if (_notification == null)
                _notification = new Command.Notification();
        }

        public bool Remove(Guid Id)
        {
            var record = ContactsDefault.Where(x => x.GuidId == Id).FirstOrDefault();
            if (record == null)
            {
                _notification.Add("Contato: Falha ao excluir, o contato não localizado pelo id informado.");
                return false;
            }
            _contactRepository.Remove(record.Id);
            return _contactRepository.Save() > 0;
        }

        public Contact Create(Contact contact)
        {
            var model = new Contact(contact);
            if (!model.IsValid()) return contact;
            _contactRepository.Add(model);
            _contactRepository.Save();
            return contact;
        }

        public async Task<Contact> CreateAsync(Contact contact)
        {
            var model = new Contact(contact);
            if (!model.IsValid()) return contact;
            await _contactRepository.AddAsync(model);
            await _contactRepository.SaveAsync();
            return contact;
        }

        public IEnumerable<Contact> GetContactsEnumerable()
        {
            return Contacts;
        }

        public IReadOnlyList<Contact> GetContactsReadOnLy()
        {
            return ContactsDefault.ToList();
        }

        public Contact AlterContact(Contact contact)
        {
            var record = ContactsDefault.Where(x => x.GuidId == contact.GuidId).FirstOrDefault();
            if (record == null || !new OnValidateDomain(contact).DomainIsValid())
            {
                _notification.Add("Contato: Falha na alteração, contato não localizado pelo id informado no objeto, verifique os dados.");
                return contact;
            }

            contact.ChanceId(record.Id);
            contact.RecordsHistory.CreationDate = record.RecordsHistory.CreationDate;
            contact.RecordsHistory.ChangeDate = DateTime.Now;
            _contactRepository.Update(contact);
            _contactRepository.Save();
            return contact;
        }
    }
}
﻿using Elyon.Domain.Command;
using Elyon.Domain.Enumerators;
using Elyon.Domain.Interface;
using Elyon.Domain.Interface.Repository;
using Elyon.Domain.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Elyon.Domain.Layer
{
    public class AddressServiceLayer : IAddressDomainLayer
    {
        private IQueryable<Address> AddressesDefault { get; set; }
        private IQueryable<Address> Addresses { get; set; }
        private readonly IAddresRepository _repository;
        protected readonly INotification _notification;
        public AddressServiceLayer(IAddresRepository addresRepository)
        {
            _repository = addresRepository;

            AddressesDefault = _repository
                .GetAllNoTracking()
                .Where(x => x.EStatus.Equals(EStatusRegister.Active) || x.Equals(EStatusRegister.Desactive));

            Addresses = _repository.GetAll();

            if (_notification == null)
                _notification = new Command.Notification();
        }
        public Address Create(Address address)
        {
            var model = new Address(address);
            if (!model.IsValid()) return address;
            _repository.Add(model);
            _repository.Save();
            return address;
        }
        public async Task<Address> CreateAsync(Address address)
        {
            var model = new Address(address);
            if (!model.IsValid()) return address;
            await _repository.AddAsync(model);
            await _repository.SaveAsync();
            return address;
        }
        public IEnumerable<Address> GetAddresses()
        {
            return AddressesDefault;
        }
        /// <summary>
        /// Use este método quando precisar carregar uma view, grid, ou retorno do tipo somente leitura, por padrão ele trás todos os indices ativos e inativos exceto excluídos.
        /// </summary>
        /// <returns></returns>
        public IReadOnlyList<Address> GetAddressesReadOnLy()
        {
            return AddressesDefault.ToList();
        }
        public bool Remove(Guid Id)
        {
            var record = AddressesDefault.Where(x => x.GuidId == Id).FirstOrDefault();
            if (record == null)
            {
                _notification.Add("Endereço: Falha ao excluir, o endereço não localizado pelo id informado.");
                return false;
            }
            _repository.Remove(record.Id);
            return _repository.Save() > 0;
        }

        public Address AlterAddress(Address address)
        {
            var record = AddressesDefault.Where(x => x.GuidId == address.GuidId).FirstOrDefault();
            if(record == null || !new OnValidateDomain(address).DomainIsValid())
            {
                _notification.Add("Endereço: Falha na alteração, endereço não localizado pelo id informado no objeto, verifique os dados.");
                return address;
            }
            
            address.ChanceId(record.Id);
            address.FormatCep(address.ZipCode);
            address.RecordsHistory.CreationDate = record.RecordsHistory.CreationDate;
            address.RecordsHistory.ChangeDate = DateTime.Now;
            _repository.Update(address);
            _repository.Save();
            return address;
        }
    }
}
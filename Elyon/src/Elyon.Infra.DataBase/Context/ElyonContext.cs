﻿using Elyon.Domain.Model;
using Elyon.Infra.DataBase.Map;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using System.IO;

namespace Elyon.Infra.DataBase.Context
{
    public class ElyonContext : DbContext
    {
        public DbSet<Address> TBAddress { get; set; }
        public DbSet<Contact> TBContact { get; set; }
        public ElyonContext(DbContextOptions dbContext) : base(dbContext)
        {

        }
        public override void Dispose()
        {
            base.Dispose();
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new AddressMap());
            modelBuilder.ApplyConfiguration(new ContactMap());
            base.OnModelCreating(modelBuilder);
        }
        public class ApplicationContextDbFactory : IDesignTimeDbContextFactory<ElyonContext>
        {
            ElyonContext IDesignTimeDbContextFactory<ElyonContext>.CreateDbContext(string[] args)
            {
                var config = new ConfigurationBuilder()
                  .SetBasePath(Directory.GetCurrentDirectory())
                  .AddJsonFile("appsettings.json")
                  .Build();

                var optionsBuilder = new DbContextOptionsBuilder<ElyonContext>();
                optionsBuilder.UseMySql<ElyonContext>(config.GetConnectionString("DefaultConnection"));
                return new ElyonContext(optionsBuilder.Options);
            }
        }
    }
}
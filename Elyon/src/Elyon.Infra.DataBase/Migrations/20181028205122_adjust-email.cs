﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Elyon.Infra.DataBase.Migrations
{
    public partial class adjustemail : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Email",
                table: "TBContact",
                newName: "CEmail");

            migrationBuilder.AlterColumn<string>(
                name: "CEmail",
                table: "TBContact",
                type: "varchar(200)",
                maxLength: 100,
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "CEmail",
                table: "TBContact",
                newName: "Email");

            migrationBuilder.AlterColumn<string>(
                name: "Email",
                table: "TBContact",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "varchar(200)",
                oldMaxLength: 100);
        }
    }
}

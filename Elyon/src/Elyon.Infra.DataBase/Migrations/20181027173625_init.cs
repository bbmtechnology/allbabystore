﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Elyon.Infra.DataBase.Migrations
{
    public partial class init : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "TBAddress",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Address_InternalCode = table.Column<string>(type: "varchar(10)", maxLength: 10, nullable: false),
                    Address_Hash = table.Column<string>(type: "varchar(190)", nullable: false),
                    AStreet = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: false),
                    AHouseNumber = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: false),
                    AZipCode = table.Column<string>(type: "varchar(9)", maxLength: 9, nullable: false),
                    ADistrict = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: false),
                    AComplement = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: true),
                    AReference = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: false),
                    ANote = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: true),
                    ACity = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: false),
                    AInitials = table.Column<string>(type: "varchar(3)", maxLength: 2, nullable: false),
                    AState = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: false),
                    ACountry = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: false),
                    AEstatus = table.Column<int>(type: "int", maxLength: 1, nullable: false),
                    DataCreation = table.Column<DateTime>(type: "Datetime", nullable: false),
                    DataChange = table.Column<DateTime>(type: "Datetime", nullable: false),
                },
                constraints: table =>
                {
                    table.PrimaryKey("AddressId", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "TBAddress");
        }
    }
}

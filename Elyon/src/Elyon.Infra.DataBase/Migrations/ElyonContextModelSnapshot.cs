﻿// <auto-generated />
using System;
using Elyon.Infra.DataBase.Context;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace Elyon.Infra.DataBase.Migrations
{
    [DbContext(typeof(ElyonContext))]
    partial class ElyonContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.1.4-rtm-31024")
                .HasAnnotation("Relational:MaxIdentifierLength", 64);

            modelBuilder.Entity("Elyon.Domain.Model.Address", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("City")
                        .IsRequired()
                        .HasColumnName("ACity")
                        .HasColumnType("varchar(100)")
                        .HasMaxLength(100);

                    b.Property<string>("Complement")
                        .HasColumnName("AComplement")
                        .HasColumnType("varchar(100)")
                        .HasMaxLength(100);

                    b.Property<string>("Country")
                        .IsRequired()
                        .HasColumnName("ACountry")
                        .HasColumnType("varchar(100)")
                        .HasMaxLength(100);

                    b.Property<string>("District")
                        .IsRequired()
                        .HasColumnName("ADistrict")
                        .HasColumnType("varchar(100)")
                        .HasMaxLength(100);

                    b.Property<int>("EStatus")
                        .HasColumnName("AEstatus")
                        .HasColumnType("int")
                        .HasMaxLength(1);

                    b.Property<string>("GuidId")
                        .IsRequired()
                        .HasConversion(new ValueConverter<string, string>(v => default(string), v => default(string), new ConverterMappingHints(size: 36)))
                        .HasColumnName("Address_Hash")
                        .HasColumnType("varchar(190)");

                    b.Property<string>("HouseNumber")
                        .IsRequired()
                        .HasColumnName("AHouseNumber")
                        .HasColumnType("varchar(100)")
                        .HasMaxLength(100);

                    b.Property<string>("Initials")
                        .IsRequired()
                        .HasColumnName("AInitials")
                        .HasColumnType("varchar(3)")
                        .HasMaxLength(2);

                    b.Property<string>("InternalCode")
                        .IsRequired()
                        .HasColumnName("Address_InternalCode")
                        .HasColumnType("varchar(10)")
                        .HasMaxLength(10);

                    b.Property<string>("Note")
                        .HasColumnName("ANote")
                        .HasColumnType("varchar(100)")
                        .HasMaxLength(100);

                    b.Property<string>("Reference")
                        .IsRequired()
                        .HasColumnName("AReference")
                        .HasColumnType("varchar(100)")
                        .HasMaxLength(100);

                    b.Property<string>("State")
                        .IsRequired()
                        .HasColumnName("AState")
                        .HasColumnType("varchar(100)")
                        .HasMaxLength(100);

                    b.Property<string>("Street")
                        .IsRequired()
                        .HasColumnName("AStreet")
                        .HasColumnType("varchar(100)")
                        .HasMaxLength(100);

                    b.Property<string>("ZipCode")
                        .IsRequired()
                        .HasColumnName("AZipCode")
                        .HasColumnType("varchar(9)")
                        .HasMaxLength(9);

                    b.HasKey("Id")
                        .HasName("AddressId");

                    b.ToTable("TBAddress");
                });

            modelBuilder.Entity("Elyon.Domain.Model.Contact", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("CellPhone")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("CCellPhone")
                        .HasColumnType("varchar(20)")
                        .HasMaxLength(9)
                        .HasDefaultValue("000000000");

                    b.Property<string>("DDDCellPhone")
                        .IsRequired()
                        .ValueGeneratedOnAdd()
                        .HasColumnName("CDDD_CellPhone")
                        .HasColumnType("varchar(2)")
                        .HasMaxLength(5)
                        .HasDefaultValue("00");

                    b.Property<string>("DDDPhone")
                        .IsRequired()
                        .ValueGeneratedOnAdd()
                        .HasColumnName("CDDD_Phone")
                        .HasColumnType("varchar(2)")
                        .HasMaxLength(2)
                        .HasDefaultValue("00");

                    b.Property<int>("EStatus")
                        .HasColumnName("CEstatus")
                        .HasColumnType("int")
                        .HasMaxLength(1);

                    b.Property<string>("Email")
                        .IsRequired()
                        .HasColumnName("CEmail")
                        .HasColumnType("varchar(200)")
                        .HasMaxLength(100);

                    b.Property<string>("GuidId")
                        .IsRequired()
                        .HasConversion(new ValueConverter<string, string>(v => default(string), v => default(string), new ConverterMappingHints(size: 36)))
                        .HasColumnName("Contact_Hash")
                        .HasColumnType("varchar(190)");

                    b.Property<string>("InternalCode")
                        .IsRequired()
                        .HasColumnName("Contact_InternalCode")
                        .HasColumnType("varchar(10)")
                        .HasMaxLength(10);

                    b.Property<string>("PersonName")
                        .IsRequired()
                        .HasColumnName("CPersonName")
                        .HasColumnType("varchar(100)")
                        .HasMaxLength(100);

                    b.Property<string>("Phone")
                        .IsRequired()
                        .ValueGeneratedOnAdd()
                        .HasColumnName("CPhone")
                        .HasColumnType("varchar(20)")
                        .HasMaxLength(8)
                        .HasDefaultValue("00000000");

                    b.HasKey("Id")
                        .HasName("ContactId");

                    b.ToTable("TBContact");
                });

            modelBuilder.Entity("Elyon.Domain.Model.Address", b =>
                {
                    b.OwnsOne("Elyon.Domain.ValuesObjects.Records", "RecordsHistory", b1 =>
                        {
                            b1.Property<int>("AddressId");

                            b1.Property<DateTime>("ChangeDate")
                                .HasColumnName("DataChange")
                                .HasColumnType("Datetime");

                            b1.Property<DateTime>("CreationDate")
                                .HasColumnName("DataCreation")
                                .HasColumnType("Datetime");

                            b1.ToTable("TBAddress");

                            b1.HasOne("Elyon.Domain.Model.Address")
                                .WithOne("RecordsHistory")
                                .HasForeignKey("Elyon.Domain.ValuesObjects.Records", "AddressId")
                                .OnDelete(DeleteBehavior.Cascade);
                        });
                });

            modelBuilder.Entity("Elyon.Domain.Model.Contact", b =>
                {
                    b.OwnsOne("Elyon.Domain.ValuesObjects.Records", "RecordsHistory", b1 =>
                        {
                            b1.Property<int>("ContactId");

                            b1.Property<DateTime>("ChangeDate")
                                .HasColumnName("DataChange")
                                .HasColumnType("Datetime");

                            b1.Property<DateTime>("CreationDate")
                                .HasColumnName("DataCreation")
                                .HasColumnType("Datetime");

                            b1.ToTable("TBContact");

                            b1.HasOne("Elyon.Domain.Model.Contact")
                                .WithOne("RecordsHistory")
                                .HasForeignKey("Elyon.Domain.ValuesObjects.Records", "ContactId")
                                .OnDelete(DeleteBehavior.Cascade);
                        });
                });
#pragma warning restore 612, 618
        }
    }
}

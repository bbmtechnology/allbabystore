﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Elyon.Infra.DataBase.Migrations
{
    public partial class addcontact : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "TBContact",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Contact_InternalCode = table.Column<string>(type: "varchar(10)", maxLength: 10, nullable: false),
                    Contact_Hash = table.Column<string>(type: "varchar(190)", nullable: false),
                    CPersonName = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: false),
                    Email = table.Column<string>(nullable: true),
                    CPhone = table.Column<string>(type: "varchar(20)", maxLength: 8, nullable: false, defaultValue: "00000000"),
                    CDDD_Phone = table.Column<string>(type: "varchar(5)", maxLength: 5, nullable: false),
                    CCellPhone = table.Column<string>(type: "varchar(20)", maxLength: 9, nullable: true, defaultValue: "000000000"),
                    CDDD_CellPhone = table.Column<string>(type: "varchar(5)", maxLength: 5, nullable: false),                    
                    CEstatus = table.Column<int>(type: "int", maxLength: 1, nullable: false),
                    DataCreation = table.Column<DateTime>(type: "Datetime", nullable: false),
                    DataChange = table.Column<DateTime>(type: "Datetime", nullable: false),                  
                },
                constraints: table =>
                {
                    table.PrimaryKey("ContactId", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "TBContact");
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Elyon.Infra.DataBase.Migrations
{
    public partial class ajustetablecontact : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "CDDD_Phone",
                table: "TBContact",
                type: "varchar(2)",
                maxLength: 2,
                nullable: false,
                defaultValue: "00",
                oldClrType: typeof(string),
                oldType: "varchar(5)",
                oldMaxLength: 5);

            migrationBuilder.AlterColumn<string>(
                name: "CDDD_CellPhone",
                table: "TBContact",
                type: "varchar(2)",
                maxLength: 5,
                nullable: false,
                defaultValue: "00",
                oldClrType: typeof(string),
                oldType: "varchar(5)",
                oldMaxLength: 5);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "CDDD_Phone",
                table: "TBContact",
                type: "varchar(5)",
                maxLength: 5,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "varchar(2)",
                oldMaxLength: 2,
                oldDefaultValue: "00");

            migrationBuilder.AlterColumn<string>(
                name: "CDDD_CellPhone",
                table: "TBContact",
                type: "varchar(5)",
                maxLength: 5,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "varchar(2)",
                oldMaxLength: 5,
                oldDefaultValue: "00");
        }
    }
}

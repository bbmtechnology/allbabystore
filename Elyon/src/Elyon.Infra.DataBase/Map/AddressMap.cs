﻿using Elyon.Domain.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Elyon.Infra.DataBase.Map
{
    public class AddressMap : IEntityTypeConfiguration<Address>
    {
        public void Configure(EntityTypeBuilder<Address> builder)
        {
            builder.HasKey(a => a.Id)
                .HasName("AddressId");

            builder.Property(a => a.GuidId)
                .HasColumnName("Address_Hash")
                .HasColumnType("varchar(190)")
                .IsRequired();

            builder.Property(a => a.InternalCode)
                .HasColumnName("Address_InternalCode")
                .HasColumnType("varchar(10)")
                .HasMaxLength(10)
                .IsRequired();

            builder.OwnsOne(c => c.RecordsHistory)
                .Property(c => c.CreationDate)
                .HasColumnName("DataCreation")
                .HasColumnType("Datetime")
                .IsRequired();

            builder.OwnsOne(c => c.RecordsHistory)
                .Property(c => c.ChangeDate)
                .HasColumnName("DataChange")
                .HasColumnType("Datetime")
                .IsRequired();

            builder.Property(a => a.EStatus)
                .HasColumnName("AEstatus")
                .HasColumnType("int")
                .HasMaxLength(1)
                .IsRequired();

            builder.Property(a => a.City)
                .HasColumnName("ACity")
                .HasColumnType("varchar(100)")
                .HasMaxLength(100)
                .IsRequired();

            builder.Property(a => a.District)
                .HasColumnName("ADistrict")
                .HasColumnType("varchar(100)")
                .HasMaxLength(100)
                .IsRequired();

            builder.Property(a => a.Complement)
                .HasColumnName("AComplement")
                .HasColumnType("varchar(100)")
                .HasMaxLength(100);

            builder.Property(a => a.Country)
                .HasColumnName("ACountry")
                .HasColumnType("varchar(100)")
                .HasMaxLength(100)
                .IsRequired();

            builder.Property(a => a.Reference)
                .HasColumnName("AReference")
                .HasColumnType("varchar(100)")
                .HasMaxLength(100)
                .IsRequired();

            builder.Property(a => a.State)
                .HasColumnName("AState")
                .HasColumnType("varchar(100)")
                .HasMaxLength(100)
                .IsRequired();

            builder.Property(a => a.Street)
                .HasColumnName("AStreet")
                .HasColumnType("varchar(100)")
                .HasMaxLength(100)
                .IsRequired();

            builder.Property(a => a.ZipCode)
                .HasColumnName("AZipCode")
                .HasColumnType("varchar(9)")
                .HasMaxLength(9)
                .IsRequired();

            builder.Property(a => a.Note)
                .HasColumnName("ANote")
                .HasColumnType("varchar(100)")
                .HasMaxLength(100);

            builder.Property(a => a.Initials)
                .HasColumnName("AInitials")
                .HasColumnType("varchar(3)")
                .HasMaxLength(2)
                .IsRequired();

            builder.Property(a => a.HouseNumber)
                .HasColumnName("AHouseNumber")
                .HasColumnType("varchar(100)")
                .HasMaxLength(100)
                .IsRequired();
        }
    }
}

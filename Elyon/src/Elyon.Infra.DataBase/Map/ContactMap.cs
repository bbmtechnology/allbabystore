﻿using Elyon.Domain.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Elyon.Infra.DataBase.Map
{
    public class ContactMap : IEntityTypeConfiguration<Contact>
    {
        public void Configure(EntityTypeBuilder<Contact> builder)
        {
            builder.HasKey(a => a.Id)
                .HasName("ContactId");

            builder.Property(a => a.GuidId)
                .HasColumnName("Contact_Hash")
                .HasColumnType("varchar(190)")
                .IsRequired();

            builder.Property(a => a.InternalCode)
                .HasColumnName("Contact_InternalCode")
                .HasColumnType("varchar(10)")
                .HasMaxLength(10)
                .IsRequired();

            builder.OwnsOne(c => c.RecordsHistory)
                .Property(c => c.CreationDate)
                .HasColumnName("DataCreation")
                .HasColumnType("Datetime")
                .IsRequired();

            builder.OwnsOne(c => c.RecordsHistory)
                .Property(c => c.ChangeDate)
                .HasColumnName("DataChange")
                .HasColumnType("Datetime")
                .IsRequired();

            builder.Property(a => a.EStatus)
                .HasColumnName("CEstatus")
                .HasColumnType("int")
                .HasMaxLength(1)
                .IsRequired();

            builder.Property(a => a.CellPhone)
                .HasColumnName("CCellPhone")
                .HasColumnType("varchar(20)")
                .HasMaxLength(9)
                .HasDefaultValue("000000000");

            builder.Property(a => a.Phone)
                .HasColumnName("CPhone")
                .HasColumnType("varchar(20)")
                .HasMaxLength(8)
                .HasDefaultValue("00000000")
                .IsRequired();

            builder.Property(a => a.PersonName)
                .HasColumnName("CPersonName")
                .HasColumnType("varchar(100)")
                .HasMaxLength(100)
                .IsRequired();

            builder.Property(a => a.Email)
              .HasColumnName("CEmail")
              .HasColumnType("varchar(200)")
              .HasMaxLength(100)
              .IsRequired();

            builder.Property(a => a.DDDCellPhone)
               .HasColumnName("CDDD_CellPhone")
               .HasColumnType("varchar(2)")
               .HasMaxLength(5)
               .HasDefaultValue("00")
               .IsRequired();

            builder.Property(a => a.DDDPhone)
               .HasColumnName("CDDD_Phone")
               .HasColumnType("varchar(2)")
               .HasMaxLength(2)
               .HasDefaultValue("00")
               .IsRequired();
        }
    }
}

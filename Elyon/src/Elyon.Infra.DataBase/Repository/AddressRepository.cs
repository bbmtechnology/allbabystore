﻿using Elyon.Domain.Interface.Repository;
using Elyon.Domain.Model;
using Elyon.Infra.DataBase.Context;

namespace Elyon.Infra.DataBase.Repository
{
    public class AddressRepository : Repository<Address>, IAddresRepository
    {
        public AddressRepository(ElyonContext context) : base(context)
        {
        }
    }
}
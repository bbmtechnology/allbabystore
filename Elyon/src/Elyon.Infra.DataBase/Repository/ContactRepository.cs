﻿using Elyon.Domain.Interface.Repository;
using Elyon.Domain.Model;
using Elyon.Infra.DataBase.Context;

namespace Elyon.Infra.DataBase.Repository
{
    public class ContactRepository : Repository<Contact>, IContactRepository
    {
        public ContactRepository(ElyonContext context) : base(context)
        {
        }
    }
}
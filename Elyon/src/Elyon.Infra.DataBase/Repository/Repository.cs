﻿using Elyon.Domain.Interface.Repository;
using Elyon.Infra.DataBase.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Elyon.Infra.DataBase.Repository
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        private readonly ElyonContext Context;
        private readonly DbSet<TEntity> DBSet;

        public Repository(ElyonContext context)
        {
            Context = context;
            DBSet = Context.Set<TEntity>();
        }

        #region Method Sync       
        public void Add(TEntity obj) => DBSet.Add(obj);
        public IQueryable<TEntity> GetAll() => DBSet;
        public IQueryable<TEntity> GetAllNoTracking() => DBSet.AsNoTracking();
        public TEntity GetById(int id) => DBSet.Find(id);
        public void Remove(int id) => DBSet.Remove(DBSet.Find(id));
        public int Save() => Context.SaveChanges();
        public void Update(TEntity obj) => DBSet.Update(obj);
        #endregion

        public async Task<int> SaveAsync() => await Context.SaveChangesAsync();
        public async Task AddAsync(TEntity obj) => await DBSet.AddAsync(obj);
        public void Dispose()
        {
            Context.Dispose();
            GC.SuppressFinalize(this);
        }
    }
}
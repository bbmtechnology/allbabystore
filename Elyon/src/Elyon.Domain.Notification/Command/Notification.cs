﻿using Elyon.Domain.Interface;
using Elyon.Domain.Notification.Entitie;
using System;
using System.Collections.Generic;
using System.Text;

namespace Elyon.Domain.Notification.Command
{
    public class Notification : INotification
    {
        private static List<Message> Notifications { get; set; }

        public Notification()
        {
            Notifications = new List<Message>();
        }

        public void Add(string message)
        {
            Notifications.Add(new Message() { Id = 0, Note = message });
        }

        List<string> INotification.Notifications()
        {
            throw new NotImplementedException();
        }
    }
}

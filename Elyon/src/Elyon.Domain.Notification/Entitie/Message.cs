﻿namespace Elyon.Domain.Notification.Entitie
{
    public sealed partial class Message
    {
        public int Id { get; set; }
        public string Note { get; set; }
    }
}
﻿using Elyon.Application.Services.Interface;
using Elyon.Application.Services.Service;
using Elyon.Domain.Command;
using Elyon.Domain.Interface;
using Elyon.Domain.Interface.Repository;
using Elyon.Domain.Layer;
using Elyon.Infra.DataBase.Context;
using Elyon.Infra.DataBase.Repository;
using Microsoft.Extensions.DependencyInjection;

namespace Elyon.Infra.CrossCutting.IoC
{
    public class NativeInjectorBootStrapper
    {
        public static void RegisterServices(IServiceCollection services)
        {
            // ASP.NET HttpContext dependency
            //services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            // Application
            services.AddScoped<IAddressAppService, AddressApplicationService>();
            services.AddScoped<IContactAppService, ContactApplicationService>();


            //Domain
            services.AddScoped<INotification, Notification>();
            services.AddScoped<IAddressDomainLayer, AddressServiceLayer>();
            services.AddScoped<IContactDomainLayer, ContactServiceLayer>();

            // Infra - Data
            services.AddScoped<IAddresRepository, AddressRepository>();
            services.AddScoped<IContactRepository, ContactRepository>();
            //services.AddScoped<ICustomerServiceDomain, CustomerDomainService>();
            //services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddScoped<ElyonContext>();
        }
    }
}
